package ky;

public class FlatService {
	int id;
	String name;

	public int getId() {
		return id;
	}

	public FlatService setId(int id) {
		this.id = id;
		return this;
	}

	public String getName() {
		return name;
	}

	public FlatService setName(String name) {
		this.name = name;
		return this;
	}

	@Override
	public String toString() {
		return "Service [id=" + id + ", name=" + name + "]";
	}

}
