package ky;

public class Invoice {
	int id;
	int year;
	int month;
	int flat_id;
	Double summa;
	String serviceOnInvoice;

	public int getId() {
		return id;
	}

	public Invoice setId(int id) {
		this.id = id;
		return this;
	}

	public int getYear() {
		return year;
	}

	public Invoice setYear(int year) {
		this.year = year;
		return this;
	}

	public int getMonth() {
		return month;
	}

	public Invoice setMonth(int month) {
		this.month = month;
		return this;
	}

	public int getFlat_id() {
		return flat_id;
	}

	public Invoice setFlat_id(int flat_id) {
		this.flat_id = flat_id;
		return this;
	}

	public Double getSumma() {
		return summa;
	}

	public Invoice setSumma(Double summa) {
		this.summa = summa;
		return this;
	}

	public String getserviceOnInvoice() {
		return serviceOnInvoice;
	}

	public Invoice setserviceOnInvoice(String service_id) {
		this.serviceOnInvoice = service_id;
		return this;
	}

	@Override
	public String toString() {
		return "Invoices [id=" + id + ", year=" + year + ", month=" + month + ", flat_id=" + flat_id + ", summa="
				+ summa + ", serviceOnInvoice=" + serviceOnInvoice + "]";
	}

}
