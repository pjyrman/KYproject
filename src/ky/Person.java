package ky;

public class Person {
	int id;
	String forname;
	String surname;
	String personalCode;
	String address;
	String email;
	String telephone;
	String bankAccount;

	public String getBankAccount() {
		return bankAccount;
	}

	public Person setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
		return this;
	}

	public int getId() {
		return id;
	}

	public Person setId(int id) {
		this.id = id;
		return this;
	}

	public String getForname() {
		return forname;
	}

	public Person setForname(String forname) {
		this.forname = forname;
		return this;
	}

	public String getSurname() {
		return surname;
	}

	public Person setSurname(String surname) {
		this.surname = surname;
		return this;
	}

	public String getPersonalCode() {
		return personalCode;
	}

	public Person setPersonalCode(String personalCode) {
		this.personalCode = personalCode;
		return this;
	}

	public String getAddress() {
		return address;
	}

	public Person setAddress(String address) {
		this.address = address;
		return this;
	}

	public String getEmail() {
		return email;
	}

	public Person setEmail(String email) {
		this.email = email;
		return this;
	}

	public String getTelephone() {
		return telephone;
	}

	public Person setTelephone(String telephone) {
		this.telephone = telephone;
		return this;
	}

	@Override
	public String toString() {
		return "Person [id=" + id + ", forname=" + forname + ", surname=" + surname + ", personalCode=" + personalCode
				+ ", address=" + address + ", email=" + email + ", telephone=" + telephone + "]";
	}

}
