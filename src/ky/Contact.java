package ky;

public class Contact extends ContactWithMessage {
	int id;
	int flatNr;
	String archives;
	Person person;

	public int getId() {
		return id;
	}

	public String getArchives() {
		return archives;
	}

	public void setArchives(String archives) {
		this.archives = archives;
	}

	public Contact setId(int id) {
		this.id = id;
		return this;
	}

	public int getFlatNr() {
		return flatNr;
	}

	public Contact setFlatNr(int flatNr) {
		this.flatNr = flatNr;
		return this;
	}

	public Person getPerson() {
		return person;
	}

	public Contact setPerson(Person person) {
		this.person = person;
		return this;
	}

	@Override
	public String toString() {
		return "Contact [id=" + id + ", flatNr=" + flatNr + ", personId=" + person + "]";
	}

}
