package ky;

public class ContactWithMessage  {
	String message;
	Contact contact;

	public Contact getContact() {
		return contact;
	}

	public ContactWithMessage setContact(Contact contact) {
		this.contact = contact;
		return this;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "ContactWithMessages [message=" + message + "]";
	}

	
}
