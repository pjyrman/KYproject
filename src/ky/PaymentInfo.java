package ky;

public class PaymentInfo {
	int flatNr;
	int payday;
	String personalcode;
	String bankAccount;
	String forname;
	String surname;
	String referenceNumber;
	Double summa;

	public int getFlatNr() {
		return flatNr;
	}

	public PaymentInfo setFlatNr(int flatNr) {
		this.flatNr = flatNr;
		return this;

	}

	public int getPayday() {
		return payday;
	}

	public PaymentInfo setPayday(int payday) {
		this.payday = payday;
		return this;
	}

	public String getPersonalcode() {
		return personalcode;
	}

	public PaymentInfo setPersonalcode(String personalcode) {
		this.personalcode = personalcode;
		return this;
	}

	public String getBankAccount() {
		return bankAccount;
	}

	@Override
	public String toString() {
		return "PaymentInfo [flatNr=" + flatNr + ", payday=" + payday + ", personalcode=" + personalcode
				+ ", bankAccount=" + bankAccount + ", forname=" + forname + ", surname=" + surname
				+ ", referenceNumber=" + referenceNumber + ", summa=" + summa + "]";
	}

	public PaymentInfo setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
		return this;
	}

	public String getForname() {
		return forname;
	}

	public PaymentInfo setForname(String forname) {
		this.forname = forname;
		return this;
	}

	public String getSurname() {
		return surname;
	}

	public PaymentInfo setSurname(String surname) {
		this.surname = surname;
		return this;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public PaymentInfo setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
		return this;
	}

	public Double getSumma() {
		return summa;
	}

	public PaymentInfo setSumma(Double summa) {
		this.summa = summa;
		return this;
	}
}
