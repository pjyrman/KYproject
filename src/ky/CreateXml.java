package ky;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import kyResource.InvoiceResource;

public class CreateXml {
	public static String createXml() {
		System.setProperty("file.encoding", "UTF8");
		Double summa = 0.0;
		int countInvoices = 0;
//tehakse stringiks kuup�ev, p�ev, kuu aasta ja eelmine kuu, 2 kohalisel kujul 
		String date = String.format("%1$td", new Date());
		String month = String.format("%1$tm", new Date());
		String year = String.format("%1$tY", new Date());
		String invoiceMonth = String.format("%02d", (Integer.parseInt(month) - 1));
		String fileXml = "C:\\Users\\Piret\\hd staff\\Kokku\\Korteriyhistu\\2017 juhatus\\earved\\earve" + year + month + ".xml";
		System.out.println(fileXml);
//Faili loomine kuhu hakatakse andmeid sisetama (new filewriter)
		
		try (FileWriter writer = new FileWriter(new File(fileXml))) {

			System.out.println("headeri kirjutamine");
			writer.append(headerXml(date, month, year));
			//invoiceResources k�ivitatakse select payments, p�ringule antakse ette aasta ja kuu
			List<PaymentInfo> paymentInfo = InvoiceResource.selectPayments(year.toString(), invoiceMonth);
//Arvutatakse valitud arvete summa ja arv 
			for (PaymentInfo payment : paymentInfo) {
				summa = summa + payment.getSumma();
				countInvoices = countInvoices + 1;
//lisatakse faili bodysse valitud v�ljad
				writer.append(bodyXml(payment, countInvoices, date, month, year, invoiceMonth));
			}
			writer.append(footerXml(summa, countInvoices));
			writer.flush();
			writer.close();
			;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return fileXml;
	}

	public static String headerXml(String day,  String month, String year) {
		String header = "<?xml version=\"1.0\"?>";
		header = header + "<E_Invoice xsi:noNamespaceSchemaLocation=\"e-invoice_ver1.11.xsd\"\n";
		header = header + "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n";
		header = header + "<Header>\n";
		header = header + "<Date>" + year + "-" + month + "-" + day + "</Date>\n";
		header = header + "<FileId>" + month + year + "</FileId>\n";
		header = header + "<AppId>EARVE</AppId>\n";
		header = header + "<Version>1.11</Version>\n";
		header = header + "<SenderId>80184286</SenderId>\n";
		header = header + "<ReceiverId>HABAEE2X</ReceiverId>\n";
		header = header + "</Header>\n";
		return header;
	}
//kommentaar 1
	public static String bodyXml(PaymentInfo payment, int countInvoices, String date, String month, String year,
			String invoiceMonth) {
		String body = "<Invoice invoiceId=\"" + invoiceMonth + year + payment.getFlatNr() + "\" serviceId=\""
				+ payment.getReferenceNumber() + "\" regNumber=\"" + payment.getPersonalcode() + "\"";
		body = body + " sellerRegnumber=\"80184286\" channelId=\"HABAEE2X\"";
		body = body + " channelAddress=\"" + payment.getBankAccount() + "\" presentment=\"YES\"";
		body = body + " invoiceGlobUniqId=\"KARBERI54_" + countInvoices + "\">\n";
		body = body + "<InvoiceParties>\n";
		body = body + " <SellerParty>\n";
		body = body + "<Name>K� K�RBERI 54</Name>\n";
		body = body + "<RegNumber>80184286</RegNumber>\n";
		body = body + "<AccountInfo>\n";
		body = body + "<IBAN>EE532200221021650661</IBAN>\n";
		body = body + "</AccountInfo>\n";
		body = body + "</SellerParty>\n";
		body = body + "<BuyerParty>\n";
		body = body + "<Name>" + payment.getForname() + " " + payment.getSurname() + "</Name>\n";
		body = body + "</BuyerParty>\n";
		body = body + "</InvoiceParties>\n";
		body = body + "<InvoiceInformation>\n";
		body = body + "<Type type=\"DEB\"/>\n";
		body = body + "<DocumentName>Arve nr.</DocumentName>\n";
		body = body + "<InvoiceNumber>" + invoiceMonth + year + payment.getFlatNr() + "</InvoiceNumber>\n";
		body = body + "<InvoiceDate>" + year + "-" + month + "-" + date + "</InvoiceDate>\n";
		body = body + "</InvoiceInformation>\n";
		body = body + "<InvoiceSumGroup>\n";
		body = body + "<TotalSum>" + payment.getSumma() + "</TotalSum>\n";
		body = body + "<Currency>EUR</Currency>\n";
		body = body + "</InvoiceSumGroup>\n";
		body = body + "<InvoiceItem>\n";
		body = body + "<InvoiceItemGroup>\n";
		body = body + "<ItemEntry>\n";
		body = body + "<Description>Korteri hooldustasud</Description>\n";
		body = body + "</ItemEntry>\n";
		body = body + "</InvoiceItemGroup>\n";
		body = body + "</InvoiceItem>\n";
		body = body + "<PaymentInfo>\n";
		body = body + "<Currency>EUR</Currency>\n";
		body = body + "<PaymentRefId>"+ payment.getReferenceNumber() + "</PaymentRefId>\n";
		body = body + "<PaymentDescription>Arve nr " + invoiceMonth + year + payment.getFlatNr()
				+ "</PaymentDescription>\n";
		body = body + "<Payable>YES</Payable>\n";
		body = body + "<PayDueDate>" + year + "-" + month + "-" + payment.getPayday() + "</PayDueDate>\n";
		body = body + "<PaymentTotalSum>" + payment.getSumma() + "</PaymentTotalSum>\n";
		body = body + "<PayerName>" + payment.getForname() + " " + payment.getSurname() + "</PayerName>\n";
		body = body + "<PaymentId>" + payment.getReferenceNumber() + "</PaymentId>\n";
		body = body + "<PayToAccount>EE532200221021650661</PayToAccount>\n";
		body = body + "<PayToName>K� K�RBERI 54</PayToName>\n";
		body = body + "</PaymentInfo>\n";
		body = body + "</Invoice>\n";
		return body;
	}

	public static String footerXml(Double summa, int countInvoices) {
		String footer = "<Footer>\n";
		footer = footer + "<TotalNumberInvoices>" + countInvoices + "</TotalNumberInvoices>\n";
		footer = footer + "<TotalAmount>" + summa + "</TotalAmount>\n";
		footer = footer + "</Footer>\n";
		footer = footer + "</E_Invoice>\n";
		return footer;
	}
}
