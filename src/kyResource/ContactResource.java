package kyResource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import ky.Contact;
import ky.ContactWithMessage;
import ky.Person;
import kyDatabaseConnection.KyDatabaseConnection;

public class ContactResource {

	public List<Contact> getAllContacts() {
		String sqlQuery = "SELECT c.id as contactId, pp.id as personId, flat_nr,forname, surname, email, telephone, personal_code, address, bank_account FROM contacts as c, persons pp WHERE c.person_id = pp.id and archives='N' ORDER BY c.flat_nr";
		List<Contact> contacts = MySqlCommands.selectListSql(sqlQuery);
		return contacts;
	}

	public ContactWithMessage addContact(Contact contact) {
		int personId = -1;
		ContactWithMessage contactMsg = checkContactPersonalcode(contact);
		if (contactMsg.getMessage() != null) {
			personId = contactMsg.getContact().getPerson().getId();
		}

		if (personId <= 0) {
			// SqlInjectionite vastu aitab prepared-statement
			Connection connection = KyDatabaseConnection.getConnection();

			PreparedStatement pstmt;
			try {
				pstmt = connection.prepareStatement(
						"INSERT INTO persons (forname, surname, email, telephone, personal_code) VALUES(?,?,?,?,?)");
				pstmt.setString(1, contact.getPerson().getForname());
				pstmt.setString(2, contact.getPerson().getSurname());
				pstmt.setString(3, contact.getPerson().getEmail());
				pstmt.setString(4, contact.getPerson().getTelephone());
				pstmt.setString(5, contact.getPerson().getPersonalCode());
			} catch (

			SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			String sqlQueryPerson = "INSERT INTO persons (forname, surname, email, telephone, personal_code) VALUES('"
					+ contact.getPerson().getForname() + "', '" + contact.getPerson().getSurname() + "', '"
					+ contact.getPerson().getEmail() + "', '" + contact.getPerson().getTelephone() + "', '"
					+ contact.getPerson().getPersonalCode() + "')";
			personId = MySqlCommands.insertSql(sqlQueryPerson);
		}
		if (personId > 0) {
			String sqlQueryContact = "INSERT INTO contacts (flat_nr, person_id) VALUES(" + contact.getFlatNr() + ", "
					+ personId + ")";
			int contactId = MySqlCommands.insertSql(sqlQueryContact);
		} else {
			contactMsg.setMessage("Person not found and contact not added. ");
		}
		return contactMsg;
	}

	public void updateContact(Contact contact) {
		String sqlQuery = "UPDATE persons SET forname ='" + contact.getPerson().getForname() + "', surname ='"
				+ contact.getPerson().getSurname() + "', telephone = '" + contact.getPerson().getTelephone()
				+ "', email='" + contact.getPerson().getEmail() + "'  WHERE id = " + contact.getPerson().getId();
		MySqlCommands.updateSql(sqlQuery);
	}

	public void deleteContact(Contact contact) {
		String sqlQuery = "UPDATE contacts SET archives = 'Y' WHERE ID =" + contact.getId();
		System.out.println(sqlQuery);
		MySqlCommands.updateSql(sqlQuery);
	}

	public ContactWithMessage checkContactPersonalcode(Contact contact) {
		ContactWithMessage contactMessages = new ContactWithMessage();
		if (contact.getPerson().getPersonalCode() != "") {
			List<Person> persons = PersonResource.getPersonByPersonalcode(contact.getPerson().getPersonalCode());
			if (persons.size() > 0) {
				contactMessages.setContact(contact).getContact().setPerson(persons.get(0));
				contactMessages
						.setMessage("Listud isik - " + persons.get(0).getForname() + " " + persons.get(0).getSurname());
			}
		}
		return contactMessages;
	}

	public ContactWithMessage checkContacName(Contact contact) {
		ContactWithMessage contactMessages = new ContactWithMessage();
		List<Person> persons = PersonResource.getPersonByName(contact.getPerson().getForname(),
				contact.getPerson().getSurname());
		if (persons.size() == 1) {
			contactMessages.setContact(contact).getContact().setPerson(persons.get(0));
			contactMessages
					.setMessage("Listud isik - " + persons.get(0).getForname() + " " + persons.get(0).getSurname());
		}
		return contactMessages;
	}

}
