package kyResource;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ky.Contact;
import ky.PaymentInfo;
import ky.Person;
import kyDatabaseConnection.KyDatabaseConnection;

public abstract class MySqlCommands {
	public static List<Contact> selectListSql(String querySql) {
		List<Contact> contactInfo = new ArrayList<>();
		try (ResultSet results = KyDatabaseConnection.getConnection().createStatement().executeQuery(querySql);) {
			while (results.next()) {
				Person person = new Person().setForname(results.getString("forname"))
						.setSurname(results.getString("surname")).setId(results.getInt("personId"))
						.setEmail(results.getString("email")).setTelephone(results.getString("telephone"))
						.setPersonalCode(results.getString("personal_code")).setAddress(results.getString("address"))
						.setBankAccount(results.getString("bank_account"));

				contactInfo.add(new Contact().setId(results.getInt("contactId")).setFlatNr(results.getInt("flat_nr"))
						.setPerson(person));
			}
		} catch (SQLException e) {
			System.out.println("Error on running query in selectListSql: " + e.getStackTrace());
		}
		System.out.println(contactInfo);
		return contactInfo;
	}

	public static List<Person> selectPersonListSql(String querySql) {
		List<Person> personInfo = new ArrayList<>();
		try (ResultSet results = KyDatabaseConnection.getConnection().createStatement().executeQuery(querySql);) {
			while (results.next()) {
				personInfo.add(new Person().setForname(results.getString("forname"))
						.setSurname(results.getString("surname")).setId(results.getInt("ID"))
						.setEmail(results.getString("email")).setTelephone(results.getString("telephone"))
						.setPersonalCode(results.getString("personal_code")).setAddress(results.getString("address"))
						.setBankAccount(results.getString("bank_account")));
			}
		} catch (SQLException e) {
			System.out.println("Error on running query in selectPersonListSql: " + e.getStackTrace());
		}
		System.out.println(personInfo);
		return personInfo;
	}

	public static List<PaymentInfo> selectListPaymentInfoSql(String querySql) {
		List<PaymentInfo> paymentInfo = new ArrayList<>();
		try (ResultSet results = KyDatabaseConnection.getConnection().createStatement().executeQuery(querySql);) {
			while (results.next()) {
				paymentInfo.add(new PaymentInfo().setFlatNr(results.getInt("flat_nr"))
						.setPayday(results.getInt("pay_date")).setPersonalcode(results.getString("personal_code"))
						.setBankAccount(results.getString("bank_account")).setForname(results.getString("forname"))
						.setSurname(results.getString("surname"))
						.setReferenceNumber(results.getString("reference_number"))
						.setSumma(results.getDouble("summa")));
			}
		} catch (SQLException e) {
			System.out.println("Error on running query: " + e.getStackTrace());
		}
		return paymentInfo;
	}
//tagastab rea ID
	public static int selectIdSql(String querySql) {
		int rowId = 0;
		Connection connection = KyDatabaseConnection.getConnection();
		try (ResultSet results = KyDatabaseConnection.getConnection().createStatement().executeQuery(querySql);) {
			while (results.next()) {
				rowId = results.getInt("id");
			}
		} catch (SQLException e) {
			System.out.println("Error on running query: " + e.getStackTrace());
		}
		KyDatabaseConnection.closeConnection(connection);
		return rowId;
	}
//edit k�su puhul sql p��rdumine
	public static void updateSql(String updateSql) {
		try {
			Integer code = KyDatabaseConnection.getConnection().createStatement().executeUpdate(updateSql);
			if (code != 1) {
				throw new SQLException("Some error on update");
			}
		} catch (SQLException e) {
			System.out.println("Error on running update query: " + e.getStackTrace());
		}
	}
	//lisamise k�su puhul sql p��rdumine
	public static int insertSql(String insertSql) {
		int keyId = 0;
		Connection connection = KyDatabaseConnection.getConnection();
		try (Statement statement = connection.createStatement()) {
			statement.executeUpdate(insertSql, Statement.RETURN_GENERATED_KEYS);
			ResultSet resultSet1 = statement.getGeneratedKeys();
			while (resultSet1.next()) {
				keyId = resultSet1.getInt(1);
			}

		} catch (SQLException e) {
			System.out.println("Error on running query: " + e.getStackTrace());
		}
		KyDatabaseConnection.closeConnection(connection);
		return keyId;
	}
	//kustutamise k�su puhul sql p��rdumine
	public static void deleteSql(String deleteSql) {
		try (Statement statement = KyDatabaseConnection.getConnection().createStatement()) {
			statement.executeUpdate(deleteSql);
		} catch (SQLException e) {
			System.out.println("Error on running query: " + e.getStackTrace());
		}
	}

}
