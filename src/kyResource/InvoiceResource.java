package kyResource;

import java.util.List;

import ky.PaymentInfo;

public class InvoiceResource {

	public static void insertInvoiceSum(String year, String month, String flat_nr, String summa, int service) {
		String sqlInsert = "INSERT INTO invoices (year, month,flat_id,summa,service_id) VALUES('" + year + "','" + month
				+ "'," + flat_nr + "," + summa + "," + service + ")";
		MySqlCommands.insertSql(sqlInsert);
	}

	public static int insertInvoiceService(String service) {
		int keyId;
		String sqlInsert = "INSERT INTO services (name) VALUES('" + service + "')";
		keyId = MySqlCommands.insertSql(sqlInsert);
		return keyId;
	}

	public static void deleteInvoiceSum(String year, String month) {
		String sqlDelete = "DELETE FROM ky_projekt.invoices WHERE YEAR ='" + year + "' AND month = '" + month + "'";
		MySqlCommands.deleteSql(sqlDelete);
	}

	public static List<PaymentInfo> selectPayments(String year, String month) {
		String sqlQuery = "SELECT f.flat_nr, forname, surname, personal_code, bank_account, reference_number, pay_date, summa \r\n"
				+ "FROM ky_projekt.payers p, persons pp, flats f, ky_projekt.invoices i, services s  \r\n"
				+ "WHERE p.flat_nr = f.id AND p.person_id= pp.id AND archives ='N' AND UPPER(s.name)=\"SUMMA\" and s.id =  i.service_id and i.flat_id = f.id AND i.year = '"
				+ year + "' And i.month ='" + month + "'";
		System.out.println(sqlQuery);
		List<PaymentInfo> payments = MySqlCommands.selectListPaymentInfoSql(sqlQuery);
		System.out.println(payments);
		return payments;
	}

	public static void invoiceExistContol(String year, String month) {
		String sqlQuery = "SELECT * FROM ky_projekt.invoices WHERE YEAR ='" + year + "' AND month = '" + month + "' ";
		int invoiceId = MySqlCommands.selectIdSql(sqlQuery);
		if (invoiceId != 0) {
			InvoiceResource.deleteInvoiceSum(year, month);
		}
	}

}
