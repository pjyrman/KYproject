package kyResource;

import java.util.List;

import ky.Person;

public class PersonResource {
	public List<Person> getAllPersons() {
		String sqlQuery = "SELECT * FROM persons";
		List<Person> persons = MySqlCommands.selectPersonListSql(sqlQuery);
		return persons;
	} 

	public static List<Person> getPersonByPersonalcode(String personalcode) {
		String sqlQuery = "SELECT * FROM persons WHERE personal_code = '"+ personalcode+"'";
		System.out.println(sqlQuery);
		List<Person> persons = MySqlCommands.selectPersonListSql(sqlQuery);
		return persons;
	}

	public static List<Person> getPersonByName(String forname, String surname) {
		String sqlQuery = "SELECT * FROM persons WHERE forname = '"+ forname+ "' AND surname = '"+ surname+"'";
		 List<Person> persons = MySqlCommands.selectPersonListSql(sqlQuery);
		return persons;
	}
}
