package kyResource;

import java.util.List;

import ky.Contact;
import ky.ContactWithMessage;
import ky.Person;

public class OwnerResource {

	public List<Contact> getAllOwners() {
		String sqlQuery = "SELECT o.id as contactId, pp.id as personId, flat_nr,forname, surname, email, telephone, personal_code, address, bank_account FROM owners o, persons pp WHERE o.person_id = pp.id and archives='N' ORDER BY o.flat_nr;\r\n"
				+ "";
		List<Contact> owners = MySqlCommands.selectListSql(sqlQuery);
		return owners;
	}

	public ContactWithMessage addOwner(Contact owner) {
		int personId = -1;
		ContactWithMessage contactMsg = checkOwnerPersonalcode(owner);
		if (contactMsg.getMessage()  != null) {
			personId = contactMsg.getContact().getPerson().getId();
		}

		if (personId <= 0) {
			String sqlQueryPerson = "INSERT INTO persons (forname, surname, email, telephone,  personal_code) VALUES('"
					+ owner.getPerson().getForname() + "', '" + owner.getPerson().getSurname() + "', '"
					+ owner.getPerson().getEmail() + "', '" + owner.getPerson().getTelephone() + "', '"
					+ owner.getPerson().getPersonalCode() + "')";
			personId = MySqlCommands.insertSql(sqlQueryPerson);
			}
			String sqlQueryOwner = "INSERT INTO owners (flat_nr, person_id) VALUES(" + owner.getFlatNr() + ", "	+ personId + ")";
			if (personId > 0) {
				int ownerId = MySqlCommands.insertSql(sqlQueryOwner);
			} else {
				contactMsg.setMessage("Person not found and contact not added. ");
			}
		return contactMsg;
	}

	public void deleteOwner(Contact owner) {
		String sqlQuery = "UPDATE owners SET archives = 'Y' WHERE ID =" + owner.getId();
		MySqlCommands.updateSql(sqlQuery);
	}

	public void updateOwner(Contact owner) {
		String sqlQuery = "UPDATE persons SET forname ='" + owner.getPerson().getForname() + "', surname ='"
				+ owner.getPerson().getSurname() + "', telephone = '" + owner.getPerson().getTelephone() + "', email='"
				+ owner.getPerson().getEmail() + "'  WHERE id = " + owner.getPerson().getId();
		MySqlCommands.updateSql(sqlQuery);
	}

	public ContactWithMessage checkOwnerPersonalcode(Contact contact) {
		ContactWithMessage contactMessages = new ContactWithMessage();
		if (contact.getPerson().getPersonalCode() != "") {
			List<Person> persons = PersonResource.getPersonByPersonalcode(contact.getPerson().getPersonalCode());
			if (persons.size() > 0) {
				contactMessages.setContact(contact).getContact().setPerson(persons.get(0));
				contactMessages
						.setMessage("Listud isik - " + persons.get(0).getForname() + " " + persons.get(0).getSurname());
			}
		}
		return contactMessages;
	}

}
