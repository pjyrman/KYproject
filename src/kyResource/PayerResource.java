package kyResource;

import java.util.List;

import ky.Contact;
import ky.ContactWithMessage;
import ky.Person;

public class PayerResource {

	public List<Contact> getAllPayers() {
		String sqlQuery = "SELECT p.id as contactId, pp.id as personId, flat_nr,forname, surname, email, telephone, personal_code, address, bank_account FROM payers p, persons pp WHERE p.person_id = pp.id and archives='N' ORDER BY p.flat_nr;";
		List<Contact> payers =MySqlCommands.selectListSql(sqlQuery);
		return payers;
	}

	public ContactWithMessage addPayer(Contact payer) {
		int personId = -1;
		ContactWithMessage contactMsg = checkPayerPersonalcode(payer);
		if (contactMsg.getMessage() != null) {
			personId = contactMsg.getContact().getPerson().getId();
		}
		if (personId <= 0) {
		String sqlQueryPerson = "INSERT INTO persons (forname, surname, email, telephone, bank_account, personal_code) VALUES('"
				+ payer.getPerson().getForname() + "', '" + payer.getPerson().getSurname() + "', '"
				+ payer.getPerson().getEmail() + "', '" + payer.getPerson().getTelephone() + "', '"
				+ payer.getPerson().getBankAccount() + "', '" + payer.getPerson().getPersonalCode() + "')";
		personId = MySqlCommands.insertSql(sqlQueryPerson);
		}
		if (personId > 0) {
			String sqlQueryPayer = "INSERT INTO payers (flat_nr, person_id) VALUES(" + payer.getFlatNr() + ", " + personId+ ")";
			int payerId = MySqlCommands.insertSql(sqlQueryPayer);
		} else {
			contactMsg.setMessage("Person not found and contact not added. ");
		}
		return contactMsg;
	}

	public void deletePayer(Contact payer) {
		String sqlQuery = "UPDATE payers SET archives = 'Y' WHERE ID =" + payer.getId();
		MySqlCommands.updateSql(sqlQuery);
	}

	public void updatePayer(Contact payer) {
		String sqlQuery = "UPDATE persons SET forname ='" + payer.getPerson().getForname() + "', surname ='"
				+ payer.getPerson().getSurname() + "', telephone = '" + payer.getPerson().getTelephone() + "', email='"
				+ payer.getPerson().getEmail() + "',bank_account='"+payer.getPerson().getBankAccount() + "' WHERE id = " + payer.getPerson().getId();
		MySqlCommands.updateSql(sqlQuery);
	}
	
	public ContactWithMessage checkPayerPersonalcode(Contact contact) {
		ContactWithMessage contactMessages = new ContactWithMessage();
		if (contact.getPerson().getPersonalCode() != "") {
			List<Person> persons = PersonResource.getPersonByPersonalcode(contact.getPerson().getPersonalCode());
			if (persons.size() > 0) {
				contactMessages.setContact(contact).getContact().setPerson(persons.get(0));
				contactMessages.setMessage("Listud isik - " + persons.get(0).getForname() + " " + persons.get(0).getSurname());
			}
		}
		return contactMessages;
	}

}
