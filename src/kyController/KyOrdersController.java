package kyController;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ky.CreateXml;
import ky.InvoceReader;

@Path("/orders")
public class KyOrdersController {

	@POST
	@Path("/{filename}")
	@Produces(MediaType.TEXT_PLAIN)
	public String invoiceReader(@PathParam("filename") String filename) {
		try {
			return InvoceReader.invoiceReader(filename);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String createXml() {
		try {
			return CreateXml.createXml();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
