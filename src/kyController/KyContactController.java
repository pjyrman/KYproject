package kyController;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ky.Contact;
import ky.ContactWithMessage;
import kyResource.ContactResource;

@Path("/contacts")
public class KyContactController {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Contact> getAllContacts() {
		ContactResource contactR = new ContactResource();
		List<Contact> contacts = contactR.getAllContacts();
		return contacts;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ContactWithMessage addContact(Contact newContact) {
		return new ContactResource().addContact(newContact);
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Contact updateContact(Contact updatedContact) {
		new ContactResource().updateContact(updatedContact);
		return (Contact) updatedContact;
	}

	@DELETE
	@Path("/{id}")
	public void deleteContact(@PathParam("id") int id) {
		new ContactResource().deleteContact(new Contact().setId(id));
	}
}
