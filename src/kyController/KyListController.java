package kyController;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ky.ListDocuments;

@Path("/list")
public class KyListController {

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public void listDocuments() {
		try {
			ListDocuments.listDocuments();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}