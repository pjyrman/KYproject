package kyController;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import ky.Contact;
import ky.ContactWithMessage;
import kyResource.OwnerResource;

@Path("/owners")
public class KyOwnerController {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Contact> getAllOwners() {
		OwnerResource ownerR = new OwnerResource();
		List<Contact> owners = ownerR.getAllOwners();
		return owners;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ContactWithMessage addOwner(Contact newOwner) {
		return new OwnerResource().addOwner(newOwner);
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Contact updateOwner(Contact updatedOwner) {
		new OwnerResource().updateOwner(updatedOwner);
		return updatedOwner;

	}

	@DELETE
	@Path("/{id}")
	public void deleteOwner(@PathParam("id") int id) {
		new OwnerResource().deleteOwner(new Contact().setId(id));
	}

}
