package kyController;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ky.Person;
import kyResource.PersonResource;

@Path("/persons")
public class KyPersonController {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Person> getAllPersons() {
		PersonResource personR = new PersonResource();
		List<Person> persons = personR.getAllPersons();
		return persons;
	}
	
		@GET
		@Path("/{personalcode}")
	@Produces(MediaType.APPLICATION_JSON)
		public List<Person> getPersonByPersonalcode(@PathParam("personalcode")String personalcode) {
			PersonResource personR = new PersonResource();
			List<Person> person = personR.getPersonByPersonalcode(personalcode);
			return person;
		}	

		@GET
		@Path("/{name}")
	@Produces(MediaType.APPLICATION_JSON)
		public List<Person> getPersonByName(@PathParam("name")String forname, String surname) {
			PersonResource personR = new PersonResource();
			List<Person> person = personR.getPersonByName(forname, surname);
			return person;
		}	
}
