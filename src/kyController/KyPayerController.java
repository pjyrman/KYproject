package kyController;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ky.Contact;
import ky.ContactWithMessage;
import kyResource.PayerResource;

@Path("/payers")
public class KyPayerController {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Contact> getAllPayers() {
		PayerResource payerR = new PayerResource();
		List<Contact> payers = payerR.getAllPayers();
		return payers;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ContactWithMessage addPayer(Contact newPayer) {
		return new PayerResource().addPayer(newPayer);
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Contact updatePayer(Contact updatedPayer) {
		new PayerResource().updatePayer(updatedPayer);
		return updatedPayer;

	}

	@DELETE
	@Path("/{id}")
	public void deletePayer(@PathParam("id") int id) {
		new PayerResource().deletePayer(new Contact().setId(id));

	}
}
