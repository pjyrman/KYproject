function createXML() {

$.ajax({
	url : "http://localhost:8080/ky_projekt/rest/orders/",
	cache : false,
	type : 'GET',
	contentType : "text/plain; charset=utf-8",
	success : function(msg) {
		alert = "<div class='alert alert-success alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-label='Close'>"
    + "<span aria-hidden='true'>&times;</span></button><strong>Success! File created at:  </strong>"+msg+"</div>";
		document.getElementById("content").innerHTML = alert;
		$(".alert").delay(20000).slideUp(200, function() {
		    $(this).alert('close');
		});
	},
	error : function(XMLHttpRequest, textStatus, errorThrown) {
		console.log("Status: " + textStatus);
		console.log("Error: " + errorThrown);
	}
});
	
}
