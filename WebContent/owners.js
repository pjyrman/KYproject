function getAllOwners() {
	$
			.getJSON(
					"http://localhost:8080/ky_projekt/rest/owners/",
					function(persons) {
						var tableContent = "<h1 id='header'>O W N E R S</h1>";
						tableContent = tableContent
								+ "<table id='example' class='table table-striped table-bordered table-hover'>";
						tableContent = tableContent
								+ "<thead>	<tr> <th><div class='input-group'><input class='form-control col-sm-3' type='number' placeholder ='Flat'  id='flatFilter' onkeyup='flatFilter()' title='Flat number'></th>	<th>Name</th><th>Personalcode</th><th>Telephone</th>	<th>E-mail</th></tr></thead><tbody>";
						for (var i = 0; i < persons.length; i++) {
							tableContent = tableContent
									+ "<tr><td class='col-sm-1'>"
									+ persons[i].flatNr + "</td><td>"
									+ persons[i].person.forname + " "
									+ persons[i].person.surname + "</td><td>"
									+ persons[i].person.personalCode
									+ "</td><td>" + persons[i].person.telephone
									+ "</td><td>" + persons[i].person.email
									+ "</td></tr>";
						}

						tableContent = tableContent + "</tbody></table>";
						document.getElementById("content").innerHTML = tableContent;
						$('#example').DataTable();

					});
}

function addOwnerForm() {
	var formContent = "<h2 id='header'>A D D  &nbsp O W N E R S</h2>";
	formContent = formContent
			+ "<form name='owners' method='post'><Table class='addTable'><tr>";
	formContent = formContent
			+ "<div class='input-group'><tr><td>Flat Nr :</td> <td><input id='flatNr' name='flatNr' class='form-control' type='number' max='200' ></div></td></tr>";
	formContent = formContent
			+ "<div class='ui-widget input-group'><tr><td style='color:red;'>Find person:  </td> <td><input id='findPerson' name='findPerson' class='form-control' onblur='fillOwnerPersonalcodeName()'></div></td></tr>";
	formContent = formContent + addPersonForm();
			formContent = formContent + "</table>";
	formContent = formContent
			+ "</br><button class='btn btn-info' type='button' onClick='validateFormOwner()'>Add</button></form>";
	document.getElementById("content").innerHTML = formContent;
	$('#findPerson').autocomplete({
		source : availablePersons
	});

}

function fillOwnerPersonalcodeName() {
	if (availablePersons.includes(document.forms["owners"]["findPerson"].value)) {
		var personInfo = document.forms["owners"]["findPerson"].value
				.split(" ");
		document.forms["owners"]["personalcode"].value = personInfo[0];
		document.forms["owners"]["forname"].value = personInfo[2];
		document.forms["owners"]["surname"].value = personInfo[3];
	} else {
		document.forms["owners"]["findPerson"].value = ""
	}
	validateFormOwner();
}

function validateFormOwner() {
	if (document.forms["owners"]["flatNr"].value == "") {
		alert = "<div class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-label='Close'>"
				+ "<span aria-hidden='true'>&times;</span></button><strong>Flatnumber must be  filled out!</strong></div>";
		document.getElementById("content1").innerHTML = alert;
		$(".alert").delay(20000).slideUp(200, function() {
			$(this).alert('close');
		});
	} else {
		if (document.forms["owners"]["findPerson"].value == "") {
			if (document.forms["owners"]["forname"].value == ""
					|| document.forms["owners"]["surname"].value == "") {
				alert = "<div class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-label='Close'>"
						+ "<span aria-hidden='true'>&times;</span></button><strong>Names must be  filled out!</strong></div>";
				document.getElementById("content1").innerHTML = alert;
				$(".alert").delay(20000).slideUp(200, function() {
					$(this).alert('close');
				});
			} else {
				if (document.forms["owners"]["personalCode"].value == "") {
					alert = "<div class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-label='Close'>"
							+ "<span aria-hidden='true'>&times;</span></button><strong>Personalcode must be  filled out!</strong></div>";
					document.getElementById("content1").innerHTML = alert;
					$(".alert").delay(20000).slideUp(200, function() {
						$(this).alert('close');
					});
				} else {
					addOwner();
				}
			}
		} else {
			addOwner();
		}
	}
}

function addOwner() {
	var newOwnerJSON = {
		"flatNr" : document.getElementById("flatNr").value,
		"person" : {
			"email" : document.getElementById("email").value,
			"forname" : document.getElementById("forname").value,
			"surname" : document.getElementById("surname").value,
			"telephone" : document.getElementById("telephone").value,
			"personalCode" : document.getElementById("personalcode").value
		}
	};
	$
			.ajax({
				url : "http://localhost:8080/ky_projekt/rest/owners/",
				cache : false,
				type : 'POST',
				data : JSON.stringify(newOwnerJSON),
				contentType : "application/json; charset=utf-8",
				success : function(addOwner) {
					if (typeof (addOwner.message) != "undefined") {
						alert = "<div class='alert alert-success alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-label='Close'>"
								+ "<span aria-hidden='true'>&times;</span></button><strong>"
								+ addOwner.message + "</strong></div>";
						document.getElementById("content1").innerHTML = alert;
						$(".alert").delay(20000).slideUp(200, function() {
							$(this).alert('close');
						})
					} else {
						alert = "<div class='alert alert-success alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-label='Close'>"
								+ "<span aria-hidden='true'>&times;</span></button><strong> Uus isik lisatud </strong></div>";
						document.getElementById("content1").innerHTML = alert;
						$(".alert").delay(20000).slideUp(200, function() {
							$(this).alert('close');
						})
					}
					getAllOwners();
				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					console.log("Status : " + textStatus);
					console.log("error :" + errorThrown)
				}
			});
}

function editOwners() {
	$
			.getJSON(
					"http://localhost:8080/ky_projekt/rest/owners/",
					function(persons) {
						var tableContent = "<h1 id='header'>E D I T  &nbsp O W N E R S</h1>";
						tableContent = tableContent
								+ "<table id='example' class='table table-striped table-bordered table-hover'>";
						tableContent = tableContent
								+ "<thead>	<tr> <th><div class='input-group'><input class='form-control col-sm-3' type='number' placeholder ='Flat'  id='flatFilter' onkeyup='flatFilter()' title='Flat number'></th>	<th>Name</th><th>Personalcode</th><th>Telephone</th>	<th>E-mail</th><th></th><th></th></tr></thead><tbody>";
						for (var i = 0; i < persons.length; i++) {
							tableContent = tableContent
									+ "<tr><td class='col-sm-1'>"
									+ persons[i].flatNr
									+ "</td><td id = 'name'>"
									+ persons[i].person.forname
									+ " "
									+ persons[i].person.surname
									+ "</td><td>"
									+ persons[i].person.personalCode
									+ "</td><td>"
									+ persons[i].person.telephone
									+ "</td><td>"
									+ persons[i].person.email
									+ "</td><td>"
									+ "<button type='button'  class='btn btn-info'  onClick='editOwner("
									+ persons[i].id
									+ ")'>Edit</button></td><td>"
									+ "<button class='btn btn-danger' type='button' data-toggle='modal' data-target='#delete' onClick='fillDeleteOwner("
									+ persons[i].id + ",\""
									+ persons[i].person.forname + "\",\""
									+ persons[i].person.surname
									+ "\")'>Delete</button></td></tr>";
						}
						tableContent = tableContent + "</tbody></table>";
						document.getElementById("content").innerHTML = tableContent;
						$('#example').DataTable();

					});
}
function editOwner(ownerId) {
	$
			.getJSON(
					"http://localhost:8080/ky_projekt/rest/owners/",
					function(persons) {
						var tableContent = "<h1 id='header'>E D I T  &nbsp O W N E R</h1>";
						tableContent = tableContent
								+ "<table id='example' class='table table-striped table-bordered table-hover'>";
						tableContent = tableContent
								+ "<thead>	<tr> <th>Flat nr</th><th>Forname</th><th>Surname</th><th>Personalcode</th>	<th>Telephone</th>	<th>E-mail</th><th></th></tr></thead><tbody>";
						for (var i = 0; i < persons.length; i++) {
							if (persons[i].id == ownerId) {
								tableContent = tableContent
										+ "<tr><td>"
										+ persons[i].flatNr
										+ "</td><td>"
										+ "<div class='input-group'><input id='forname' name='forname' class='form-control' type='text' value="
										+ persons[i].person.forname
										+ "></div></td><td>"
										+ "<div class='input-group'><input id='surname' name='surname' class='form-control' type='text' value="
										+ persons[i].person.surname
										+ "></div></td><td>"
										+ "<div class='input-group'><input id='personalcode' name='personalcode' class='form-control' onblur='controlPC()' type='number' value="
										+ persons[i].person.personalCode
										+ "></div></td><td>"
										+ "<div class='input-group'><input id='telephone' name='telephone' class='form-control' type='text' value="
										+ persons[i].person.telephone
										+ "></div></td><td>"
										+ "<div class='input-group'><input id='email' name='email' class='form-control' type='text' value="
										+ persons[i].person.email
										+ "></td><td>"
										+ "<button type='button' class='btn btn-info' onClick='updateOwner("
										+ persons[i].id + ", "
										+ persons[i].person.id
										+ ")'>Save</button></td></tr>";
							} else {
								tableContent = tableContent + "<tr><td>"
										+ persons[i].flatNr + "</td><td>"
										+ persons[i].person.forname
										+ "</td><td>"
										+ persons[i].person.surname
										+ "</td><td>"
										+ persons[i].person.personal_code
										+ "</td><td>"
										+ persons[i].person.telephone
										+ "</td><td>" + persons[i].person.email
										+ "</td><td></td></tr>";
							}
						}

						tableContent = tableContent + "</tbody></table>";
						document.getElementById("content").innerHTML = tableContent;
						$('#example').DataTable();

					});
}

function updateOwner(ownerId, personId) {
	var editOwnerJSON = {
		// "flatNr" : document.getElementById("flatNr").value,
		"id" : ownerId,
		"person" : {
			"id" : personId,
			"email" : document.getElementById("email").value,
			"telephone" : document.getElementById("telephone").value,
			"personalCode" : document.getElementById("personalcode").value,
			"forname" : document.getElementById("forname").value,
			"surname" : document.getElementById("surname").value,
		}
	};
	$.ajax({
		url : "http://localhost:8080/ky_projekt/rest/owners/",
		cache : false,
		type : 'PUT',
		data : JSON.stringify(editOwnerJSON),
		contentType : "application/json; charset=utf-8",
		success : function() {
			editOwners();
			// $('#deleteCarModal').modal('toggle');
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("Status: " + textStatus);
			console.log("Error: " + errorThrown);
		}
	});
}

function fillDeleteOwner(ownerId, ownerForname, ownerSurname) {
	document.getElementById("deleteBody").innerHTML = "<p>Delete "
			+ ownerForname + " " + ownerSurname + "?</p>";
	document.getElementById("buttons").innerHTML = "<button type='button' class='btn btn-yes' data-dismiss='modal' onClick='deleteOwner("
			+ ownerId
			+ ")'>Yes</button>"
			+ "<button type='button' class='btn btn-no' data-dismiss='modal'>No</button>";
}

function deleteOwner(ownerId) {
	$.ajax({
		url : "http://localhost:8080/ky_projekt/rest/owners/" + ownerId,
		cache : false,
		type : 'DELETE',
		contentType : "application/json; charset=utf-8",
		success : function() {
			editOwners();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("Status: " + textStatus);
			console.log("Error: " + errorThrown);
		}
	});
}
