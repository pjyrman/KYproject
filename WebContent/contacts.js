function getAllContacts() {
	$
			.getJSON(
					"http://localhost:8080/ky_projekt/rest/contacts/",
					function(persons) {
						var tableContent = "<h1 id='header'>C O N T A C T S</h1>";
						tableContent = tableContent
								+ "<div><table id='example' class='table table-striped table-bordered table-hover'>";
						tableContent = tableContent
								+ "<thead>	<tr> <th><div class='input-group'><input class='form-control col-sm-3' type='number' placeholder ='Flat'  id='flatFilter' onkeyup='flatFilter()' title='Flat number'></th>	<th>Name</th><th>Telephone</th>	<th>E-mail</th></tr></thead><tbody></div>";
						for (var i = 0; i < persons.length; i++) {
							tableContent = tableContent
									+ "<tr><td class='col-sm-1'>"
									+ persons[i].flatNr + "</td><td>"
									+ persons[i].person.forname + " "
									+ persons[i].person.surname + "</td><td>"
									+ persons[i].person.telephone + "</td><td>"
									+ persons[i].person.email + "</td></tr>";
						}
						tableContent = tableContent + "</tbody></table></div>";
						document.getElementById("content").innerHTML = tableContent;
						$('#example').DataTable();
					});
}

function addContactForm() {
	var formContent = "<h2 id='header'>A D D  &nbsp  C O N T A C T</h2>";
	formContent = formContent
			+ "<form name='contacts' method='post'><Table class='addTable'><tr>"
	formContent = formContent
			+ "<div class='input-group'><tr><td>Flat Nr :</td> <td><input id='flatNr' name='flatNr' class='form-control' type='number' max='200'></div></td></tr>";
	formContent = formContent
			+ "<div class='ui-widget input-group'><tr><td style='color:red;'>Find person: </td> <td><input id='findPerson' name='findPerson' class='form-control' onblur='fillPersonalcodeName()'></div></td></tr>";
	formContent = formContent
			+ "<div class='input-group'><tr><td>Forname : </td> <td><input id='forname' name='forname' class='form-control' type='text'></div></td></tr>";
	formContent = formContent
			+ "<div class='input-group'><tr><td>Surname : </td> <td><input id='surname' name='surname' class='form-control' type='text'></div></td></tr>";
	formContent = formContent
			+ "<div class='input-group'><tr><td>Personal Code: </td> <td><input  id='personalcode' name='personalcode' class='form-control' onblur='controlPC()' type ='text'></div></td></tr>";
	formContent = formContent
			+ "<div class='input-group'><tr><td>Phone : </td> <td><input id='telephone' name='telephone' class='form-control' type='number'></div></td></tr>";
	formContent = formContent
			+ "<div class='input-group'><tr><td>E-Mail: </td> <td><input  id='email' name='email' class='form-control' type ='email'></div></td></tr>";
	formContent = formContent 
			+ "</table></br><button type='button' class='btn btn-info' onClick='validateFormContact()''>Add</button>";
	document.getElementById("content").innerHTML = formContent;
	$('#findPerson').autocomplete({
		source : availablePersons
	});
}

function fillPersonalcodeName() {
	if (availablePersons.includes(document.forms["contacts"]["findPerson"].value)) {
	var personInfo = document.forms["contacts"]["findPerson"].value.split(" ");
	document.forms["contacts"]["personalcode"].value = personInfo[0];
	document.forms["contacts"]["forname"].value = personInfo[2];
	document.forms["contacts"]["surname"].value = personInfo[3];
	} else {
		document.forms["contacts"]["findPerson"].value = ""
	}

	validateFormContact();
}

function validateFormContact() {
	if (document.forms["contacts"]["flatNr"].value == "") {
		alert = alertMessage("danger","Flatnumber must be  filled out!");
		document.getElementById("content1").innerHTML = alert;
		$(".alert").delay(20000).slideUp(200, function() {
			$(this).alert('close');
		});
	} else {
		if (document.forms["contacts"]["findPerson"].value == "") {
			if (document.forms["contacts"]["forname"].value == ""
					|| document.forms["contacts"]["surname"].value == "") {
				alert = "<div class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-label='Close'>"
						+ "<span aria-hidden='true'>&times;</span></button><strong>Names must be  filled out!</strong></div>";
				document.getElementById("content1").innerHTML = alert;
				$(".alert").delay(20000).slideUp(200, function() {
					$(this).alert('close');
				});
			} else {
				if (document.forms["contacts"]["telephone"].value == ""
						&& document.forms["contacts"]["email"].value == "") {
					alert = "<div class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-label='Close'>"
							+ "<span aria-hidden='true'>&times;</span></button><strong>Phone or email must be  filled out!</strong></div>";
					document.getElementById("content1").innerHTML = alert;
					$(".alert").delay(20000).slideUp(200, function() {
						$(this).alert('close');
					});
				} else {
					addContact();
				}
			}
		} else {
			addContact();
		}
	}
}

function addContact() {
	var newContactJSON = {
		"flatNr" : document.getElementById("flatNr").value,
		"person" : {
			"email" : document.getElementById("email").value,
			"telephone" : document.getElementById("telephone").value,
			"forname" : document.getElementById("forname").value,
			"surname" : document.getElementById("surname").value,
			"personalCode" : document.getElementById("personalcode").value
		}
	};
	$
			.ajax({
				url : "http://localhost:8080/ky_projekt/rest/contacts/",
				cache : false,
				type : 'POST',
				data : JSON.stringify(newContactJSON),
				contentType : "application/json; charset=utf-8",
				success : function(addContact) {
					if (typeof (addContact.message) != "undefined") {
						alert = "<div class='alert alert-success alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-label='Close'>"
								+ "<span aria-hidden='true'>&times;</span></button><strong>"
								+ addContact.message + "'</strong></div>";
						document.getElementById("content1").innerHTML = alert;
						$(".alert").delay(20000).slideUp(200, function() {
							$(this).alert('close');
						});
					} else {
						alert = "<div class='alert alert-success alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-label='Close'>"
								+ "<span aria-hidden='true'>&times;</span></button><strong> Uus isik lisatud </strong></div>";
						document.getElementById("content1").innerHTML = alert;
						$(".alert").delay(20000).slideUp(200, function() {
							$(this).alert('close');
						})
					}

					getAllContacts();
				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					console.log("Status : " + textStatus);
					console.log("error :" + errorThrown)
				}
			});
}

function editContacts() {
	$
			.getJSON(
					"http://localhost:8080/ky_projekt/rest/contacts/",
					function(persons) {
						var tableContent = "<h1 id='header'>E D I T  &nbsp  C O N T A C T S</h1>";
						tableContent = tableContent
								+ "<table id='example' class='table table-striped table-bordered table-hover'>";
						tableContent = tableContent
								+ "<thead>	<tr> <th><div class='input-group'><input class='form-control col-sm-3' type='number' placeholder ='Flat'  id='flatFilter' onkeyup='flatFilter()' title='Flat number'></th>	<th>Name</th><th>Telephone</th>	<th>E-mail</th><th></th><th></th></tr></thead><tbody>";
						for (var i = 0; i < persons.length; i++) {
							tableContent = tableContent
									+ "<tr><td class='col-sm-1'>"
									+ persons[i].flatNr
									+ "</td><td>"
									+ persons[i].person.forname
									+ " "
									+ persons[i].person.surname
									+ "</td><td>"
									+ persons[i].person.telephone
									+ "</td><td>"
									+ persons[i].person.email
									+ "</td><td>"
									+ "<button type='button' class='btn btn-info' onClick='editContact("
									+ persons[i].id
									+ ")'>Edit</button></td><td>"
									+ "<button class='btn btn-danger' type='button' data-toggle='modal' data-target='#delete' onClick='fillDeleteContact("
									+ persons[i].id + ",\""
									+ persons[i].person.forname + "\",\""
									+ persons[i].person.surname
									+ "\")'>Delete</button></td></tr>";
						}
						tableContent = tableContent + "</tbody></table>";
						document.getElementById("content").innerHTML = tableContent;
						$('#example').DataTable();
					});
}

function editContact(contactId) {
	$
			.getJSON(
					"http://localhost:8080/ky_projekt/rest/contacts/",
					function(persons) {
						var tableContent = "<h1 id='header'>E D I T  &nbsp  C O N T A C T</h1>";
						tableContent = tableContent
								+ "<table><table id='example' class='table table-striped table-bordered table-hover'>";
						tableContent = tableContent
								+ "<thead>	<tr> <th>Flat nr</th><th>Forname</th><th>Surname</th><th>Telephone</th>	<th>E-mail</th><th></th></tr></thead><tbody>";
						for (var i = 0; i < persons.length; i++) {
							if (persons[i].id == contactId) {
								tableContent = tableContent
										+ "<tr><td>"
										+ persons[i].flatNr
										+ "</td><td>"
										+ "<div class='input-group'><input id='forname' name='forname' class='form-control' type='text' value="
										+ persons[i].person.forname
										+ "></div></td><td>"
										+ "<div class='input-group'><input id='surname' name='surname' class='form-control' type='text' value="
										+ persons[i].person.surname
										+ "></div></td><td>"
										+ "<div class='input-group'><input id='telephone' name='telephone' class='form-control' type='text' value="
										+ persons[i].person.telephone
										+ "></div></td><td>"
										+ "<div class='input-group'><input id='email' name='email' class='form-control' type='text' value="
										+ persons[i].person.email
										+ "></div></td><td>"
										+ "<button type='button'class='btn btn-info' onClick='updateContact("
										+ persons[i].id + ", "
										+ persons[i].person.id
										+ ")'>Save</button></td></tr>";
							} else {
								tableContent = tableContent + "<tr><td>"
										+ persons[i].flatNr + "</td><td>"
										+ persons[i].person.forname
										+ "</td><td>"
										+ persons[i].person.surname
										+ "</td><td>"
										+ persons[i].person.telephone
										+ "</td><td>" + persons[i].person.email
										+ "</td><td></td></tr>";
							}
						}

						tableContent = tableContent + "</tbody></table>";
						document.getElementById("content").innerHTML = tableContent;
						$('#example').DataTable();

					});
}

function updateContact(contactId, personId) {

	var editContactJSON = {
		"id" : contactId,
		"person" : {
			"id" : personId,
			"email" : document.getElementById("email").value,
			"telephone" : document.getElementById("telephone").value,
			"forname" : document.getElementById("forname").value,
			"surname" : document.getElementById("surname").value,
		}
	};
	$.ajax({
		url : "http://localhost:8080/ky_projekt/rest/contacts/",
		cache : false,
		type : 'PUT',
		data : JSON.stringify(editContactJSON),
		contentType : "application/json; charset=utf-8",
		success : function() {
			editContacts();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("Status: " + textStatus);
			console.log("Error: " + errorThrown);
		}
	});
}

function fillDeleteContact(contactId, contactForname, contactSurname) {
	document.getElementById("deleteBody").innerHTML = "<p>Delete "
			+ contactForname + " " + contactSurname + "?</p>";
	document.getElementById("buttons").innerHTML = "<button type='button' class='btn btn-yes' data-dismiss='modal' onClick='deleteContact("
			+ contactId
			+ ")'>Yes</button>"
			+ "						<button type='button' class='btn btn-no' data-dismiss='modal'>No</button>";
}

function deleteContact(contactId) {
	$.ajax({
		url : "http://localhost:8080/ky_projekt/rest/contacts/" + contactId,
		cache : false,
		type : 'DELETE',
		contentType : "application/json; charset=utf-8",
		success : function() {
			editContacts();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("Status: " + textStatus);
			console.log("Error: " + errorThrown);
		}
	});
}
