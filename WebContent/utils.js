function controlPC() {
	var personalcode = document.getElementById("personalcode").value;
	var OK = true;
	if (parseInt(personalcode.substring(0, 1)) < 3
			|| parseInt(personalcode.substring(0, 1)) > 6) {
		OK = false;
	}
	if (parseInt(personalcode.substring(1, 3)) < 00
			|| parseInt(personalcode.substring(1, 3)) > 99) {
		OK = false;
	}
	if (parseInt(personalcode.substring(3, 5)) < 1
			|| parseInt(personalcode.substring(3, 5)) > 12) {
		OK = false;
	}
	if (parseInt(personalcode.substring(5, 7)) < 0
			|| parseInt(personalcode.substring(5, 7)) > 31) {
		OK = false;
	}
	if (OK == false) {
		alert = "<div class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-label='Close'>"
				+ "<span aria-hidden='true'>&times;</span></button><strong>CHECK personal code!</strong></div>";
		document.getElementById("content1").innerHTML = alert;
		$(".alert").delay(20000).slideUp(200, function() {
			$(this).alert('close');
		});

	}
	;
}

function flatFilter() {
	var flat, filter, table, tr, td, i;
	flat = document.getElementById("flatFilter");
	filter = flat.value;
	table = document.getElementById("example");
	tr = table.getElementsByTagName("tr");
	for (i = 0; i < tr.length; i++) {
		td = tr[i].getElementsByTagName("td")[0];
		if (td) {
			if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
				tr[i].style.display = "";
			} else {
				tr[i].style.display = "none";
			}
		}
	}
}

var availablePersons = [];
getAllPersons();

function getAllPersons() {
	$.getJSON("http://localhost:8080/ky_projekt/rest/persons/", function(
			persons) {
		for (var i = 0; i < persons.length; i++) {
			if (persons[i].personalCode != "") {
				availablePersons.push(persons[i].personalCode + " - "
						+ persons[i].forname + " " + persons[i].surname);
			}
		}

	});
}

function alertMessage(alertType, message) {
	return "<div class='alert alert-"
			+ alertType
			+ " alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-label='Close'>"
			+ "<span aria-hidden='true'>&times;</span></button><strong>"
			+ alertType + "</strong></div>";
}

function addPersonForm() {
	return "<div class='input-group'><tr><td>Forname : </td> <td><input id='forname' name='forname' class='form-control' type='text'></div></td></tr>"
			+ "<div class='input-group'><tr><td>Surname : </td> <td><input id='surname' name='surname' class='form-control' type='text'></div></td></tr>"
			+ "<div class='input-group'><tr><td>Personal Code: </td> <td><input id='personalcode' name='personalCode' class='form-control' onblur='controlPC()' type ='number'></div></td></tr>"
			+ "<div class='input-group'><tr><td>Phone : </td> <td><input id='telephone' name='telephone' class='form-control' type='number'></div></td></tr>"
			+ "<div class='input-group'><tr><td>E-Mail: </td> <td><input  id='email' name='email' class='form-control' type ='email'></div></td></tr>";
	+ "<div class='input-group'><tr><td>Address: </td> <td><input  id='address' name='address' class='form-control' type ='address'></div></td></tr>";
	+ "<div class='input-group'><tr><td>Bank account: </td> <td><input  id='bankAccount' name='bankAccount' class='form-control' type ='bankAccount'></div></td></tr>";
}