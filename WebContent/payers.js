function getAllPayers() {
	$
			.getJSON(
					"http://localhost:8080/ky_projekt/rest/payers/",
					function(persons) {
						var tableContent = "<h1 id='header'>P A Y E R S</h1>";
						tableContent = tableContent
								+ "<table id='example' class='table table-striped table-bordered'>";
						tableContent = tableContent
								+ "<thead>	<tr> <th><div class='input-group'><input class='form-control col-sm-3' type='number' placeholder ='Flat'  id='flatFilter' onkeyup='flatFilter()' title='Flat number'></th>	<th>Name</th><th>Personalcode</th><th>Bank account</th><th>Telephone</th>	<th>E-mail</th></tr></thead><tbody>";
						for (var i = 0; i < persons.length; i++) {
							tableContent = tableContent
									+ "<tr><td class='col-sm-1'>"
									+ persons[i].flatNr + "</td><td>"
									+ persons[i].person.forname + " "
									+ persons[i].person.surname + "</td><td>"
									+ persons[i].person.personalCode
									+ "</td><td>"
									+ persons[i].person.bankAccount
									+ "</td><td>" + persons[i].person.telephone
									+ "</td><td>" + persons[i].person.email
									+ "</td></tr>";
						}

						tableContent = tableContent + "</tbody></table>";
						document.getElementById("content").innerHTML = tableContent;
						$('#example').DataTable();
					});
}

function editPayers() {
	$
			.getJSON(
					"http://localhost:8080/ky_projekt/rest/payers/",
					function(persons) {
						var tableContent = "<h1 id='header'>E D I T  &nbsp  P A Y E R S</h1>";
						tableContent = tableContent
								+ "<table id='example' class='table table-striped table-bordered table-hover'>";
						tableContent = tableContent
								+ "<thead>	<tr> <th><div class='input-group'><input class='form-control col-sm-3' type='number' placeholder ='Flat'  id='flatFilter' onkeyup='flatFilter()' title='Flat number'></th>	<th>Name</th><th>Personalcode</th><th>Bank account</th><th>Telephone</th>	<th>E-mail</th><th></th><th></th></tr></thead><tbody>";
						for (var i = 0; i < persons.length; i++) {
							tableContent = tableContent
									+ "<tr><td class='col-sm-1'>"
									+ persons[i].flatNr
									+ "</td><td>"
									+ persons[i].person.forname
									+ " "
									+ persons[i].person.surname
									+ "</td><td>"
									+ persons[i].person.personalCode
									+ "</td><td>"
									+ persons[i].person.bankAccount
									+ "</td><td>"
									+ persons[i].person.telephone
									+ "</td><td>"
									+ persons[i].person.email
									+ "</td><td>"
									+ "<button type='button'  class='btn btn-info'  onClick='editPayer("
									+ persons[i].id
									+ ")'>Edit</button></td><td>"
									+ "<button class='btn btn-danger' type='button' data-toggle='modal' data-target='#delete' onClick='fillDeletePayer("
									+ persons[i].id + ",\""
									+ persons[i].person.forname + "\",\""
									+ persons[i].person.surname
									+ "\")'>Delete</button></td></tr>";
						}

						tableContent = tableContent + "</tbody></table>";
						document.getElementById("content").innerHTML = tableContent;
						$('#example').DataTable();
					});
}

function addPayerForm() {
	var formContent = "<h2 id='header'>A D D  &nbsp  P A Y E R</h2>";
	formContent = formContent
			+ "<div class='input-group'><form name='payers' method='post'><Table class='addTable'><tr>";
	formContent = formContent
			+ "<div class='input-group'><tr><td>Flat Nr :</td> <td><input id='flatNr' name='flatNr' class='form-control' type='number' max='200' ></div></td></tr>";
	formContent = formContent
			+ "<div class='ui-widget input-group'><tr><td style='color:red;'>Find person: </td> <td><input id='findPerson' name='findPerson' class='form-control' onblur='fillPayerPersonalcodeName()'></div></td></tr>";
	formContent = formContent+addPersonForm();
	formContent = formContent + "</table>" + "</br><button type='button' class='btn btn-info' onClick='validateFormPayer()'>Add payer</button></form>";
	document.getElementById("content").innerHTML = formContent;
	$('#findPerson').autocomplete({
		source : availablePersons
	});
}

function fillPayerPersonalcodeName() {
	if (availablePersons.includes(document.forms["payers"]["findPerson"].value)) {
	var personInfo = document.forms["payers"]["findPerson"].value.split(" ");
	document.forms["payers"]["personalcode"].value = personInfo[0];
	document.forms["payers"]["forname"].value = personInfo[2];
	document.forms["payers"]["surname"].value = personInfo[3];
	} else {
		document.forms["payers"]["findPerson"].value = ""
	}
	validateFormPayer();
}

function validateFormPayer() {
	if (document.forms["payers"]["flatNr"].value == "") {
		alert = "<div class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-label='Close'>"
				+ "<span aria-hidden='true'>&times;</span></button><strong>Flatnumber must be  filled out!</strong></div>";
		document.getElementById("content1").innerHTML = alert;
		$(".alert").delay(20000).slideUp(200, function() {
			$(this).alert('close');
		});
	} else {
		if (document.forms["payers"]["findPerson"].value == "") {
			if (document.forms["payers"]["forname"].value == ""
					|| document.forms["payers"]["surname"].value == "") {
				alert = "<div class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-label='Close'>"
						+ "<span aria-hidden='true'>&times;</span></button><strong>Names must be  filled out!</strong></div>";
				document.getElementById("content1").innerHTML = alert;
				$(".alert").delay(20000).slideUp(200, function() {
					$(this).alert('close');
				});
			} else {
				if (document.forms["payers"]["bankAccount"].value == "") {
					alert = "<div class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-label='Close'>"
							+ "<span aria-hidden='true'>&times;</span></button><strong>Bankaccount must be  filled out!</strong></div>";
					document.getElementById("content1").innerHTML = alert;
					$(".alert").delay(20000).slideUp(200, function() {
						$(this).alert('close');
					});
				} else {
					if (document.forms["payers"]["personalCode"].value == "") {
						alert = "<div class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-label='Close'>"
								+ "<span aria-hidden='true'>&times;</span></button><strong>Personalcode must be  filled out!</strong></div>";
						document.getElementById("content1").innerHTML = alert;
						$(".alert").delay(20000).slideUp(200, function() {
							$(this).alert('close');
						});
					} else {
						addPayer();
					}
				}
			}
		} else {
			addPayer();
		}
	}
}

function addPayer() {

	var newPayerJSON = {
		"flatNr" : document.getElementById("flatNr").value,
		"person" : {
			"email" : document.getElementById("email").value,
			"forname" : document.getElementById("forname").value,
			"surname" : document.getElementById("surname").value,
			"telephone" : document.getElementById("telephone").value,
			"bankAccount" : document.getElementById("bankAccount").value,
			"personalCode" : document.getElementById("personalcode").value
		}
	};

	$
			.ajax({
				url : "http://localhost:8080/ky_projekt/rest/payers/",
				cache : false,
				type : 'POST',
				data : JSON.stringify(newPayerJSON),
				contentType : "application/json; charset=utf-8",
				success : function(addPayer) {
					if (typeof (addPayer.message) != "undefined") {
						alert = "<div class='alert alert-success alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-label='Close'>"
								+ "<span aria-hidden='true'>&times;</span></button><strong>"
								+ addPayer.message + "'</strong></div>";
						document.getElementById("content1").innerHTML = alert;
						$(".alert").delay(20000).slideUp(200, function() {
							$(this).alert('close');
						});
					} else {
						alert = "<div class='alert alert-success alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-label='Close'>"
								+ "<span aria-hidden='true'>&times;</span></button><strong> Uus isik lisatud </strong></div>";
						document.getElementById("content1").innerHTML = alert;
						$(".alert").delay(20000).slideUp(200, function() {
							$(this).alert('close');
						})
					}
					getAllPayers();
				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					console.log("Status : " + textStatus);
					console.log("error :" + errorThrown)
				}
			});
}
function editPayer(payerId) {
	$
			.getJSON(
					"http://localhost:8080/ky_projekt/rest/payers/",
					function(persons) {
						var tableContent = "<h1 id='header'>E D I T  &nbsp  P A Y E R</h1>";
						tableContent = tableContent
								+ "<table id='payerEdit' class='table table-striped table-bordered table-hover'>";
						tableContent = tableContent
								+ "<thead>	<tr> <th>Flat nr</th><th>Forname</th><th>Surname</th><th>Telephone</th><th>Personalcode</th><th>Bankaccount</th><th>E-mail</th><th> </th></tr></thead><tbody>";
						for (var i = 0; i < persons.length; i++) {
							if (persons[i].id == payerId) {
								tableContent = tableContent
										+ "<tr><td id='flatNr'>"
										+ persons[i].flatNr
										+ "</td><td>"
										+ "<div class='input-group'><input id='forname' class='form-control' name='forname' type='text' value='"
										+ persons[i].person.forname
										+ "'></div></td><td>"
										+ "<div class='input-group'><input id='surname' name='surname' class='form-control' type='text' value="
										+ persons[i].person.surname
										+ "></div></td><td>"
										+ "<div class='input-group'><input id='telephone' name='telephone' class='form-control' type='text' value="
										+ persons[i].person.telephone
										+ "></div></td><td>"
										+ "<div class='input-group'><input id='personalcode' name='personalcode' class='form-control' onblur='controlPC()' type='number' value="
										+ persons[i].person.personalCode
										+ "></div></td><td>"
										+ "<div class='input-group'><input id='bankaccount' name='bankaccount' class='form-control' type='text' value="
										+ persons[i].person.bankAccount
										+ "></div></td><td>"
										+ "<div class='input-group'><input id='email' name='email' class='form-control' type='text' value="
										+ persons[i].person.email
										+ "></div></td><td>"
										+ "<button type='button' class='btn btn-info' onClick='updatePayer("
										+ persons[i].id + ", "
										+ persons[i].person.id
										+ ")'>Save</button></td></tr>";
							} else {
								tableContent = tableContent + "<tr><td>"
										+ persons[i].flatNr + "</td><td>"
										+ persons[i].person.forname
										+ "</td><td>"
										+ persons[i].person.surname
										+ "</td><td>"
										+ persons[i].person.personal_code
										+ "</td><td>"
										+ persons[i].person.bank_account
										+ "</td><td>"
										+ persons[i].person.telephone
										+ "</td><td>" + persons[i].person.email
										+ "</td><td></td></tr>";

							}
						}

						tableContent = tableContent + "</tbody></table>";
						document.getElementById("content").innerHTML = tableContent;
						$('#payerEdit').DataTable();

					});
}

function updatePayer(payerId, personId) {

	var editPayerJSON = {
		// "flatNr" : parseInt(document.getElementById("flatNr").innerText),
		"id" : payerId,
		"person" : {
			"id" : personId,
			"email" : document.getElementById("email").value,
			"telephone" : document.getElementById("telephone").value,
			"bankAccount" : document.getElementById("bankaccount").value,
			"personalCode" : document.getElementById("personalcode").value,
			"forname" : document.getElementById("forname").value,
			"surname" : document.getElementById("surname").value,
		}
	};
	$.ajax({
		url : "http://localhost:8080/ky_projekt/rest/payers/",
		cache : false,
		type : 'PUT',
		data : JSON.stringify(editPayerJSON),
		contentType : "application/json; charset=utf-8",
		success : function() {
			getAllPayers();
			// $('#deleteCarModal').modal('toggle');
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("Status: " + textStatus);
			console.log("Error: " + errorThrown);
		}
	});
}

function fillDeletePayer(payerId, payerForname, payerSurname) {
	document.getElementById("deleteBody").innerHTML = "<p>Delete "
			+ payerForname + " " + payerSurname + "?</p>";
	document.getElementById("buttons").innerHTML = "<button type='button' class='btn btn-yes' data-dismiss='modal' onClick='deletePayer("
			+ payerId
			+ ")'>Yes</button>"
			+ "						<button type='button' class='btn btn-no' data-dismiss='modal'>No</button>";
}

function deletePayer(payerId) {
	$.ajax({
		url : "http://localhost:8080/ky_projekt/rest/payers/" + payerId,
		cache : false,
		type : 'DELETE',
		contentType : "application/json; charset=utf-8",
		success : function() {
			editPayers();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("Status: " + textStatus);
			console.log("Error: " + errorThrown);
		}
	});
}
